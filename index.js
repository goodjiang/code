var myVue = new Vue({
  el: '#app',
  data: {
    time: '',
    ss: '',
    date: ''
  },
  mounted() {
    setInterval(() => {
      let year = dayjs().format('YYYY年MM月DD日');
      this.date = dayjs().format('YYYY-MM-DD');
      let am = dayjs().format('A') == 'AM' ? '上午' : '下午';
      let hh = dayjs().format('HH');
      let mm = dayjs().format('mm');
      let ss = dayjs().format('ss');
      this.time = year + ' ' + am + hh + ':' + mm + ':';
      this.ss = ss;
    }, 1000);
  }
});
